public class Cliente 
{
	private String _nombre;
	private int[] _carrito;
	
	public Cliente(String nombre, int[] carroCompra) 
	{
		super();
		this._nombre = nombre;
		this._carrito = carroCompra;
	}

	public String getNombre() 
	{
		return _nombre;
	}

	public int productosComprados() 
	{
		return _carrito.length;
	}
	
	public int producto(int i)
	{
		return _carrito[i];
	}
}