public class VersionRunneable implements Runnable
{
	private Cliente _cliente;
	private Caja _caja;
	private long _initialTime;
	
	public VersionRunneable (Cliente cliente, Caja caja, long initialTime)
	{
		_caja = caja;
		_cliente = cliente;
		_initialTime = initialTime;
	}

	@Override
	public void run() 
	{
		_caja.procesarCompra(_cliente, _initialTime);
	}
	
	public static void main(String[] args) 
	{
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 2, 2, 1, 5, 2, 3 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 1, 3, 5, 1, 1 });
		
		Caja caja1 = new Caja("Caja 1");
		Caja caja2 = new Caja("Caja 2");
		
		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
		
		Runnable proceso1 = new VersionRunneable(cliente1, caja1, initialTime);
		Runnable proceso2 = new VersionRunneable(cliente2, caja2, initialTime);
		
		new Thread(proceso1).start();
		new Thread(proceso2).start();
	}
}