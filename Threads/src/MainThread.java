public class MainThread 
{
	public static void main(String[] args) 
	{
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 2, 2, 1, 5, 2, 3 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 1, 3, 5, 1, 1 });

		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
		CajaThread caja1 = new CajaThread("Caja 1", cliente1, initialTime);
		CajaThread caja2 = new CajaThread("Caja 2", cliente2, initialTime);

		caja1.start();
		caja2.start();
	}
} 