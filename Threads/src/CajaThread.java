
public class CajaThread extends Thread 
{
	private String _nombre;
	private Cliente _cliente;
	private long _initialTime;

	public CajaThread(String nombre, Cliente cliente, long initialTime) 
	{
		_nombre = nombre;
		_cliente = cliente;
		_initialTime = initialTime;
	}

	@Override
	public void run() 
	{
		System.out.println("La caja " + _nombre + " COMIENZA A PROCESAR LA COMPRA DEL CLIENTE " 
					+ _cliente.getNombre() + " EN EL TIEMPO: " 
					+ (System.currentTimeMillis() - _initialTime) / 1000 
					+ "seg");

		for (int i = 0; i < _cliente.productosComprados(); i++) 
		{ 
				esperarXsegundos(_cliente.producto(i)); 
				System.out.println("La caja " + _nombre + " Procesado el producto " + (i + 1) +  
				" ->Tiempo: " + (System.currentTimeMillis() - _initialTime) / 1000 + 
				"seg");
		}

		System.out.println("La caja " + _nombre + " HA TERMINADO DE PROCESAR " 
						+ _cliente.getNombre() + " EN EL TIEMPO: " 
						+ (System.currentTimeMillis() - _initialTime) / 1000 
						+ "seg");
	}

	private void esperarXsegundos(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

}