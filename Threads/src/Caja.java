
public class Caja 
{
	private String _nombre;

	public Caja(String nombre) 
	{
		_nombre = nombre;
	}

	public void procesarCompra(Cliente cliente, long initialTime) 
	{
		System.out.println("La caja " + _nombre + 
				" COMIENZA A PROCESAR LA COMPRA DEL CLIENTE " + cliente.getNombre() + 
				" EN EL TIEMPO: " + (System.currentTimeMillis() - initialTime) / 1000	+
				"seg");

		for (int i = 0; i < cliente.productosComprados(); i++) 
		{ 
				this.esperarXsegundos(cliente.producto(i)); 
				System.out.println("La caja " + _nombre + " Procesado el producto " + (i + 1) +  
						" ->Tiempo: " + (System.currentTimeMillis() - initialTime) / 1000 + 
						"seg");
		}

		System.out.println("La caja " + _nombre + " HA TERMINADO DE PROCESAR " + 
				cliente.getNombre() + " EN EL TIEMPO: " + 
				(System.currentTimeMillis() - initialTime) / 1000 + "seg");
	}

	private void esperarXsegundos(int segundos) 
	{
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
}