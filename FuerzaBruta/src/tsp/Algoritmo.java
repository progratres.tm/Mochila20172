package tsp;

public class Algoritmo 
{
	public static Recorrido resolver(Instancia instancia)
	{
		Generador generador = new Generador(instancia);
		Recorrido elMejor = null;
		
		for (Recorrido actual: generador.generar())
		{
			if (elMejor == null || actual.distancia() < elMejor.distancia())
				elMejor = actual;
		}
		
		return elMejor;
	}

	public static void main(String[] args)
	{
		
			Instancia instancia = new Instancia(4);
			
			instancia.agregarDistancia(0, 1, 70);
			instancia.agregarDistancia(0, 2, 14);
			instancia.agregarDistancia(0, 3, 35);
			instancia.agregarDistancia(1, 2, 30);
			instancia.agregarDistancia(1, 3, 34);
			instancia.agregarDistancia(2, 3, 12);			

			Recorrido optimo = resolver(instancia);
			
			System.out.print("Mejor recorrido: " + optimo);
			System.out.println(" Distancia: " + optimo.distancia());
	}
}
