package tsp;

import java.util.LinkedList;

public class Generador 
{
	private Instancia _instancia;
	private int _n;
	
	public Generador(Instancia inst)
	{
		_instancia = inst;
		_n = _instancia.getCiudades();
	}
	
	//Generar todo los recorridos
	public LinkedList<Recorrido> generar()
	{
		LinkedList<Recorrido> ret = new LinkedList<Recorrido>();
		
		Recorrido recorrido = new Recorrido(_instancia);
		
		recorrido.agregar(0);
		
		completar(recorrido, ret);
		
		return ret;
	}
	
	//Proceso recursivo
	
	public void completar(Recorrido recorrido, LinkedList<Recorrido> ret)
	{
		//Caso Base: el recorrido contiene todas las ciudades
		if (recorrido.tama�o() == _n)
		{
			ret.add(recorrido.clonar());
			
			return;
		}
		
		//Caso Recursivo: agrego todas las posibles ciudades al recorrido
		for (int i=0; i<_n; ++i)
		{
			if (recorrido.contiene(i) == false)
			{
				recorrido.agregar(i);
				completar(recorrido, ret);
				
				recorrido.eliminarUltima(); //La ultima ciudad del recorrido es la ciudad i
			}
		}
	}
}
