package tsp;

public class Instancia 
{
	private int _n;
	private double[][] _distancia;
	
	public Instancia (int ciudades)
	{
		_n = ciudades;
		_distancia = new double [_n][_n];
	}
	
	public void agregarDistancia(int origen, int destino, double dist)
	{
		chequeoCiudades(origen, destino);
	
		_distancia[origen][destino] = dist;
		_distancia[destino][origen] = dist;
	}
	
	public double getDistancia(int origen, int destino)
	{
		chequeoCiudades(origen, destino);

		return _distancia[origen][destino];
	}
	
	private void chequeoCiudades(int origen, int destino) 
	{
		if (origen < 0 || origen >= _n || destino < 0 || destino >= _n)
			throw new IllegalArgumentException("El origen y el destino deben estar entre 0 y n-1. Origen = " + origen + " destino = " + destino);
	}

	public int getCiudades() 
	{
		return _n;
	}
}
