package tsp;

import java.util.ArrayList;

public class Recorrido 
{
	// Representa un recorrido por todas las ciudades

	private Instancia _instancia;

	// Las ciudades del recorrido
	private ArrayList<Integer> _ciudades;

	public Recorrido(Instancia inst)
	{
		_instancia = inst;
		_ciudades = new ArrayList<Integer>();
	}

	public void agregar(int i)
	{
		_ciudades.add(i);
	}

	public int tama�o()
	{
		return _ciudades.size();
	}

	public boolean contiene(int i) 
	{
		return _ciudades.contains(i);
	}

	public void eliminarUltima() 
	{
		_ciudades.remove(tama�o()-1);
	}

	public Recorrido clonar() 
	{
		Recorrido clon = new Recorrido(_instancia);
		for (int i=0; i < tama�o(); i++)
			clon.agregar(_ciudades.get(i));

		return clon;
	}

	public double distancia() 
	{
		double ret = 0;
		for(int i=0; i<_ciudades.size()-1; ++i)
		{
			int origen = _ciudades.get(i);
			int destino = _ciudades.get(i+1);

			ret += _instancia.getDistancia(origen, destino);

		}

		int ultima = _ciudades.get( _ciudades.size()-1 );
		int primera = _ciudades.get(0);

		ret += _instancia.getDistancia(ultima, primera);

		return ret;	
	}

	// toString
	@Override public String toString()
	{
		String ret = "";
		for(Integer x: _ciudades)
			ret += x + " ";

		return ret + _ciudades.get(0);
	}
}
