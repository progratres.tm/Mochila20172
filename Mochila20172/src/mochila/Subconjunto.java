package mochila;

import java.util.HashSet;
import java.util.Set;

public class Subconjunto 
{
	private Set<Objeto> objetos;
	
	public Subconjunto()
	{
		objetos = new HashSet<>();	
	}
	
	public void agregar(Objeto obj)
	{
		objetos.add(obj);
	}
	
	public void sacar(Objeto obj) 
	{
		objetos.remove(obj);	
	}
	
	public Subconjunto clonar()
	{
		Subconjunto s = new Subconjunto();
		for (Objeto o: objetos)
			s.agregar(o);
		return s;
	}
	
	@Override
	public String toString()
	{
		String s = "{  ";
		for (Objeto o : objetos)
		{
			s += o.nombre() + " ";
		}
		return s + "}";
	}
	
	public boolean tieneMayorBeneficioQue (Subconjunto s)
	{
		return s == null || this.beneficio() >= s.beneficio();
	}

	public double beneficio() 
	{
		double ret = 0;
		for (Objeto o: objetos)
			ret += o.beneficio();
		return ret;
	}
	
	public double peso()
	{
		double ret = 0;
		for (Objeto o: objetos)
			ret += o.peso();
		return ret;
	}
}
