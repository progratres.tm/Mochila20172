package mochila;

public class Mochila 
{
	private Instancia instancia;
	private Subconjunto subconjunto;
	private Subconjunto mejorEncontrado;

	private int _hojas;
	private int _podas;
	
	public Mochila(Instancia inst)
	{
		instancia = inst;
		subconjunto = new Subconjunto();
		mejorEncontrado = null;
	}

	public Subconjunto resolver()
	{
		_hojas = 0;
		_podas = 0;
		
		generarDesde(0);
		
		return mejorEncontrado;
	}
	private void generarDesde(int k) 
	{
		//Caso Base: el subconjunto se pasa de la capacidad, podando!
		if (subconjunto.peso() > instancia.capacidad())
		{
			_podas++;
			return;
		}
		
		//Caso Base: use todos los objetos, estoy en una hoja
		if (k == instancia.cantidadDeObjetos())
		{
			_hojas++;
			analizarActual();
			return;
		}
		
		Objeto obj = instancia.objeto(k);
		subconjunto.agregar(obj);
		generarDesde(k+1);
		
		subconjunto.sacar(obj);
		generarDesde(k+1);

	}

	private void analizarActual() 
	{
		if(subconjunto.peso() <= instancia.capacidad()
			&& subconjunto.tieneMayorBeneficioQue(mejorEncontrado))
			mejorEncontrado = subconjunto.clonar();
	}

	public int hojas()
	{
		return _hojas;
	}
	
	public int podas()
	{
		return _podas;
	}
}
