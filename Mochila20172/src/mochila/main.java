package mochila;

import java.util.Random;

public class main {

	public static void main(String[] args) 
	{
		
		double peso = 15;
		
		/*
		Instancia instancia = new Instancia(peso);
		Mochila mochila = new Mochila(instancia);
		instancia.agregarObjeto(new Objeto("Linterna",2,6));
		instancia.agregarObjeto(new Objeto("Encendedor",1,10));
		instancia.agregarObjeto(new Objeto("Comida",5,10));
		instancia.agregarObjeto(new Objeto("Bebida",3,8));
		instancia.agregarObjeto(new Objeto("Ropa",3,4));
		instancia.agregarObjeto(new Objeto("Repelente",1,2));
		instancia.agregarObjeto(new Objeto("Bolsa de Dormir",5,5));
		instancia.agregarObjeto(new Objeto("Cubiertos",1,2));
		instancia.agregarObjeto(new Objeto("Plato",2,2));
		instancia.agregarObjeto(new Objeto("Taza",1,1));
		instancia.agregarObjeto(new Objeto("Cantimplora",1,7));
		instancia.agregarObjeto(new Objeto("Equipo de Mate",4,6));
		
		Subconjunto optimo = mochila.resolver();
		
		System.out.printf("Beneficio = %f\n", optimo.beneficio() );
		System.out.printf( " Subconjunto = %s\n", optimo);
		*/
		for(int n=0; n<=30; n++)
		{
			Instancia instancia = aLeatoria(n);
			Mochila mochila = new Mochila(instancia);
			mochila.resolver();
			
			System.out.println(" n = " + n + " hojas = " + mochila.hojas() + " podas = " + mochila.podas() );
		}
	}

	private static Instancia aLeatoria(int n) 
	{
		Instancia instancia = new Instancia (2*n);
		Random random = new Random(0);
		for(int i = 0; i<n; i++)
		{
			int peso = random.nextInt(10)+1;
			int benef = random.nextInt(10)+1;
			Objeto aleatorio = new Objeto("Obj "+i, peso, benef);
			instancia.agregarObjeto(aleatorio);
		}
		
		return instancia;
	}

}
